var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var vendors = './bower_components/';
var destpath = './static/hospital/';
var nodepath = './node_modules/';

plugins.jshint = require('gulp-jshint');
plugins.concat = require('gulp-concat');
plugins.streamqueue = require('streamqueue');
plugins.sass = require('gulp-sass');
plugins.uglify = require('gulp-uglify');
plugins.concat = require('gulp-rename');
plugins.plumber = require('gulp-plumber');
plugins.gutil = require('gulp-util');
plugins.cleanCSS = require('gulp-clean-css');
plugins.stripDebug = require('gulp-strip-debug');
plugins.notify     = require('gulp-notify');
plugins.fs = require("fs");
browserify = require('browserify');
plugins.vueify = require('vueify');

var onError = function(err) {
    console.log(err);
}

gulp.task('libscripts', function() {
    return plugins.streamqueue({ objectMode: true },
        //  gulp.src(vendors +'PACE/pace.js'),
        gulp.src(nodepath +'jquery-slimscroll/jquery.slimscroll.min.js'),
        gulp.src(vendors +'slicknav/dist/jquery.slicknav.min.js'),
      //  gulp.src(vendors +'jquery sliding menu/js/jquery.sliding.menu.js'),
        gulp.src(destpath +'vendors/js/sliding-menu.js'),
        gulp.src(vendors +'counter-up/jquery.counterup.min.js'),
        gulp.src(vendors +'circliful/js/circliful.min.js'),
        gulp.src(vendors +'velocity/velocity.min.js'),
        gulp.src(vendors +'NumberProgressBar/number-pb.js'),
        gulp.src(vendors +'fakeLoader/fakeLoader.min.js'),
        gulp.src(vendors +'intl-tel-input/build/js/intlTelInput.min.js'),
        gulp.src(vendors +'intl-tel-input/build/js/utils.js')
		)    
         .pipe(plugins.plumber({ errorHandler: onError }))
        .pipe(plugins.concat('lib.js'))
        //.pipe(plugins.stripDebug())
        .pipe(gulp.dest(destpath +'js'))
        .pipe(plugins.rename('lib.min.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(destpath +'js'))
        .pipe(plugins.notify({ message: 'Lib Scripts task complete' }));
        //~ .pipe(plugins.plumber(function(error) {
		      //~ // Output an error message
			  //~ plugins.gutil.log(plugins.gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
			  //~ // emit the end event, to properly end the task
			  //~ this.emit('lib scripts finished');
			//~ })
		//~ );
       
});

gulp.task('libcss', function() {
    return plugins.streamqueue({ objectMode: true },
       // gulp.src(vendors +'foundation-icon-fonts/foundation-icons.css'),
       // gulp.src(vendors +'font-awesome/css/font-awesome.min.css'),
       // gulp.src(vendors +'Dripicons/css/dripicon.css'),
       // gulp.src(vendors +'PACE/css/themes/orange/pace-theme-flash.css'),
        gulp.src(vendors +'slicknav/dist/slicknav.css'),
        gulp.src(vendors +'intl-tel-input/build/css/intlTelInput.css'),
        gulp.src(vendors +'fakeLoader/fakeLoader.css')
        
    )
          .pipe(plugins.plumber({
				errorHandler: onError
			}))
        .pipe(plugins.concat('lib.css'))
        .pipe(gulp.dest(destpath +'css'))
        .pipe(plugins.rename('lib.min.css'))
        .pipe(plugins.cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(destpath +'css'))
        .pipe(plugins.notify({ message: 'Lib Css task complete' }));
        
        
       
});

gulp.task('foundationcss', function() {
    return plugins.streamqueue({ objectMode: true },
     
        gulp.src(vendors +'foundation-sites/dist/foundation.min.css'),
       // gulp.src(vendors +'foundation-sites/dist/foundation-flex.min.css'),
        gulp.src(nodepath +'reflex-grid/css/reflex.min.css')
        
    )
          .pipe(plugins.plumber({
				errorHandler: onError
			}))
        .pipe(plugins.concat('framework.css'))
        .pipe(gulp.dest(destpath +'css'))
        .pipe(plugins.rename('framework.min.css'))
        .pipe(plugins.cleanCSS({compatibility: 'ie10'}))
        .pipe(gulp.dest(destpath +'css'))
        .pipe(plugins.notify({ message: 'Foundation Css task complete' }));
        
        
       
});


gulp.task('copy', function() {
   
   gulp.src(vendors +'foundation-icon-fonts/**/*')
          .pipe(gulp.dest(destpath +'fonts/foundation-icon-fonts'));
   gulp.src(vendors +'font-awesome/**/*')
          .pipe(gulp.dest(destpath +'fonts/font-awesome'));
   gulp.src(vendors +'Dripicons/**/*')
          .pipe(gulp.dest(destpath +'fonts/Dripicons'));
   gulp.src(vendors +'typicons.font/**/*')
          .pipe(gulp.dest(destpath +'fonts/typicons.font'));
          
   //~ gulp.src(vendors +'foundation-sites/dist/foundation.css')
          //~ .pipe(gulp.dest(destpath +'css'));
          
   gulp.src(vendors +'foundation/scss/**/*')
          .pipe(gulp.dest(destpath +'scss/resources/foundation'));
          
   gulp.src(vendors +'foundation-sites/scss/**/*')
          .pipe(gulp.dest(destpath +'scss/resources/foundation-sites'));
          
   gulp.src(vendors +'foundation-sites/dist/foundation.min.js')
          .pipe(gulp.dest(destpath +'js'));
          
   gulp.src(vendors +'what-input/what-input.min.js')
          .pipe(gulp.dest(destpath +'js'));
   gulp.src(vendors +'jquery/dist/jquery.js')
          .pipe(gulp.dest(destpath +'js'));
   gulp.src(vendors +'waypoints/lib/jquery.waypoints.min.js')
          .pipe(gulp.dest(destpath +'js'));
   gulp.src(vendors +'PACE/**/*')
          .pipe(gulp.dest(destpath +'vendors/PACE'));
          
  // plugins.notify({ message: 'Copy task complete' });
       
});


gulp.task('lint', function() {
    return gulp.src(destpath +'js/*.js')
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(destpath +'scss/app.scss')
        .pipe(plugins.sass())
        .pipe(gulp.dest(destpath +'css'))
        .pipe(plugins.notify({ message: 'Sass task complete' }));
});

// Compile Our Vue js
gulp.task('vueify', function() {
   return  browserify(destpath +'appjs/app.js')
  .transform(plugins.vueify)
  .bundle()
  .pipe(plugins.fs.createWriteStream(destpath +"js/build.js"))
 // .pipe(plugins.notify({ message: 'vueify task complete' }));
});

// Default Task
gulp.task('default', ['libscripts' , 'libcss' , 'copy' , 'foundationcss' ,'sass' , 'vueify' ]);


