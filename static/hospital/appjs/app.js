// app.js
var Vue = require('vue')
global.$ = global.jQuery = require('jquery')
require('foundation-sites')
require('what-input')
var PACE = require('PACE')
var fakeLoader = require('fakeLoader')
var Cookies2 = require('js-cookie')


//Define the components here 
var SidebarMenu = require('./components/sidebar/sidebarmenu.vue')
var dashboardOverview = require('./components/dashboard/dashboard.vue')
var loginApp = require('./components/login/login.vue')
var TopbarMenu = require('./components/topbar/topbar.vue')
var patients = require('./components/patients/patients.vue')
var newPatient = require('./components/patients/newpatient.vue')
var allPatients = require('./components/patients/allpatients.vue')

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRFToken'] =  Cookies2.get('csrftoken')  ;

// Inialize Vue router 
var VueRouter = require('vue-router')

Vue.use(VueRouter)


var router = new VueRouter({
    hashbang: false,
    linkActiveClass: 'active'
});

var map = {
 //~ '*': {
        //~ component: Vue.extend({template: '<h4>404 Not found</h4>'})
    //~ },
     //~ '/': {
        //~ component : loginApp
    //~ },
    
     '/': {
		name : 'dashboard',
        component : dashboardOverview
    },
      '/patients': {
	name : 'patients',
    component: patients,
    subRoutes: {
	  
      '/newpatient': {
        name : 'newpatient',
        component: newPatient
      },
      '/allpatients': {
        name : 'allpatients',
        component: allPatients
      }
    }
  }

}

var routes = map

var App = Vue.extend({
   data: function() {
    return {
      routes: routes
    }
  },
  components: {
     sidebarmenu : SidebarMenu ,
     login : loginApp, 
     topbar : TopbarMenu 
   
    
  },
   methods: {
	 
    
  },
  ready:function(){
	
	$(document).foundation();  
	//PACE.start();
	$("#preloader").fakeLoader();
   
  },
   created: function() {
  
  }
  
});

router.map(map)

// Start the App
router.start(App, 'body');

