from django.http import HttpResponse , JsonResponse 
from django.shortcuts import render , redirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods
from django.core import serializers
import json


# Create your views here.
@require_http_methods(["GET", "POST"])
def index(request):
  if request.user.is_authenticated():
     #return redirect('/dashboard/')
     return render(request,'dashboard/index.html',context_instance=RequestContext(request))
  else:
    if request.method == 'GET':
       return render(request,'hospital/login.html',context_instance=RequestContext(request))
    elif  request.is_ajax() or request.method == 'POST':
        #errors = []
        ret = {}
        #data = serializers.serialize("json",errors)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        username = body['username']
        password = body['password']
        user = authenticate(username=username, password=password)
        
        if user is not None:
			if user.is_active:
				login(request , user)
				ret['status']= ['ok']
				return JsonResponse(json.dumps(ret),safe=False)
				#return redirect('/dashboard/')
			else :
				#errors.append('account is disabled')
				ret['errors']= ['account is disabled']
				return JsonResponse(json.dumps(ret),safe=False)
        else:
			#errors.append(['login error'])
			ret['errors']= ['login error']
			ret['username']= username
			
			return JsonResponse(json.dumps(ret),safe=False)

def logoutapp(request):
    logout(request)
    return redirect('/')

