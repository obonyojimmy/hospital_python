# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-17 19:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0002_auto_20160317_0448'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Info',
            new_name='Patient',
        ),
    ]
