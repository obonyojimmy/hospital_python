from __future__ import unicode_literals
from django.core.validators import RegexValidator

from django.db import models

# Create your models here.

class Patient(models.Model):
	#id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    dob = models.DateTimeField('Patient Age Date of Birth')
    dod = models.DateTimeField('Patient Date of Death',null=True, blank=True)
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    gender = models.CharField(max_length=200)
    nationality = models.CharField(max_length=200)
    colour = models.CharField(max_length=200)
    email = models.EmailField(blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], blank=True ,max_length=15) # validators should be a list
